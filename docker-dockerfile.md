# Eigene Docker-Images erzeugen

<span class="hidden-text">
https://oer-informatik.de/docker-install-mssql
</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Container sind soetwas wie ein kleiner Bruder der virtuellen Maschinen: schlanker, agiler, wendiger. Das bekannteste Tool, um Container zu administrieren ist Docker. Dieses Tutorial beschäftigt sich mit den Grundlagen der Containernutzung mit Docker._





```powershell
PS> docker container exec -u USER CONTAINERID COMMAND
# führt auf dem aktiven Container mit der CONTAINERID
# als Benutzer USER den Befehl COMMAND aus
```

```powershell
PS> docker image pull IMAGENAME # holt das DockerImage IMAGENAME aus der Registry
```



```powershell
PS> docker container run -it IMAGENAME COMMAND
# mit der Option -it startet docker im interaktiven Modus, sofern vom Befehl
# COMMAND Interaktionen erwartet werden (z.B. bash-prompt) bleiben diese
# geöffnet
```



## Ein eigenes DockerImage erstellen:

Wenn eine Weile an einem Container gearbeitet wurde wurden einige Dateien angepasst.
Um genauer herauszufinden welche Dateien angepasst wurden nutzt man den folgenden Befehl:
```bash
$ docker container diff CONTAINERID
# zeigt alle Änderungen zwischen dem zugrunde liegenden Image und
# dem DockerContainer an.
```
Sofern die Änderungen in einem neues lokalen Image gespeichert werden sollen geht dies wie folgt:
```powershell
PS>  docker container commit CONTAINERID
# Erstellt aus dem Container CONTAINERID ein lokales Image, das alle Änderungen enthält
```
```powershell
PS>  docker image tag IMAGEID "<meintag>"
# Einen neuen Tag vergeben. Die IMAGEID erhält man per docker image ls
```

## Images mit Dockerfile erzeugen

Um ein eigenes aktualisierbares Image zu erzeugen nutzt man den deklarativen Ansatz über Dockerfiles. Es wird eine Textdatei "Dockerfile" erstellt mit folgendem Inhalt (Bereiche in spitzen Klammern müssen individualisiert werden):

```dockerfile
FROM <image, dass dem neu erzeugten zugrunde liegt>
RUN <Befehl, der bei der Containererzeugung ausgeführt werden soll>
COPY <lokaler Pfad> <Pfad im Container>
WORKDIR <Verzeichnis, in dem die folgenden Befehle innerhalb des Containers ausgeführt werden>
CMD ["<Befehl>","<Argumente>"]
```
Aus diesem Dockerfile kann ein neues benamtes Image erstellt werden:

```powershell
PS>  docker image build -t <NameDesImages>:v0.1 .
# Erstellt Image mit gegebenem Namen aus Dockerfile im angegebenen Verzeichnis (hier: ".")
```
Wie kann man weitere Informationen zu Images erhalten?
```powershell
PS>  docker image history IMAGEID
# Welche Images und Vorgänger hat das jeweilige Image?
```

```powershell
PS>  docker image inspect IMAGENAME
# Infos zum Image als JSON
```

In einem Dockerfile kann z.B. auch weitere Software in den Container installiert werden, z.B.:
```dockerfile
USER root
RUN apt-get -y update
RUN apt-get -y install vim nano
```

### Fragen:
- Wann ist ein DockerFile einem Image vorzuziehen - und wann nicht?

[comment]: # (Im Deployment können ggf. über Aktualisierungen Fehler oder nicht erfüllte Abhängigkeiten ins Release geraten - hier sollte ein Image gewählt werden, wenn nicht gewährleistet werden kann, dass die Akzeptanztest hinreichend sind.)

- Wie kann man herausfinden, ob zwei Images eine gemeinsame (identische) Grundlage haben?

- Warum ist der Begriff "immutable" im Zusammenhang mit Containern so wichtig? Was verhalten sich mit Images, Dockerfile, Container und Layer dazu?

[comment]: # (Image und Layer sind immutable)

- Wann werden bei der Image-Erzeugung per Dockerfile Layer erzeugt?

[comment]: # (jeweils nach FROM, RUN und COPY)

## Dockerfile

### Docker Entrypoint und COMMAND

## Dockerhub nutzen

Nach einem konkreten Image im Dockerhub suchen:
```powershell
PS> docker search IMAGE_NAME
```

## Links und weitere Informationen

* [How to für das MS-SQL-Server Docker Image](https://learn.microsoft.com/de-de/sql/linux/quickstart-install-connect-docker?view=sql-server-ver16&pivots=cs1-bash)

* [Docker Hub zu MS SQL-Server](https://hub.docker.com/_/microsoft-mssql-server)

* [Customize-MS-SQL mit DOCKERFILE](https://github.com/microsoft/mssql-docker/tree/master/linux/preview/examples/mssql-customize)


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/]().

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
